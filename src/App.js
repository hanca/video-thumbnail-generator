import React, { useEffect, useState } from 'react';

const App = () => {
  let video, canvas
  const [context, setContext] = useState(null)
  let w, h, ratio;

  useEffect(() => {
    video = document.querySelector('video');
    canvas = document.querySelector('canvas');

    setContext(canvas.getContext('2d'))
    video.addEventListener('loadedmetadata', () => {
      ratio = video.videoWidth / video.videoHeight;
      w = video.videoWidth - 100;
      h = parseInt(w / ratio, 10);
      canvas.width = w;
      canvas.height = h;
    }, false)
  }, [context])

  const snap = () => {
    context.fillRect(0, 0, w, h);
    context.drawImage(video, 0, 0, w, h);
  }
  return (
    <>
      <video id="myvideo" controls="controls" width="400" height="150" data-setup="{}" >
        <source src="http://podcast.rickygervais.com/rickyandpepe_twitter.mp4" type="video/mp4" />
      </video>
      <button id="snap" onClick={snap} >Snap Photo</button>
      <canvas id="canvas" width="640" height="480"></canvas>
    </>
  );
}

export default App;
